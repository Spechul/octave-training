# how to run with jupyter on windows
- Go and install octave from here https://ftp.gnu.org.ua/gnu/octave/windows/
- add folder with octave-cli.exe to PATH (for example ...\Octave-6.1.0\mingw64\bin)
- python -m pip install octave_kernel
- python -m octave_kernel install
- python -m jupyter notebook and go work with it