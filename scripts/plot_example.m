x = 0:0.1:2*pi;
y = 0:0.1:2*pi;
z = sin(x)' * sin(y);
mesh(x, y, z); 
xlabel("x ->");
ylabel("y ->");
zlabel("z ->");
title("3-D waves");